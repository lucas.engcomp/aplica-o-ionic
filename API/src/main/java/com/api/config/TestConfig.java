package com.api.config;


import com.api.domain.Categoria;
import com.api.domain.Produto;
import com.api.respositories.CategoriaRepository;
import com.api.respositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.Arrays;

@Configuration
@Profile("test")
public class TestConfig implements CommandLineRunner {

    @Autowired
    private CategoriaRepository categoriaRepository;

    @Autowired
    private ProdutoRepository produtoRepository;

    @Override
    public void run(String... args) throws Exception {

        Categoria categoria = new Categoria(null, "Informática");
        Categoria categoria2 = new Categoria(null, "Escritório");

        Produto produto = new Produto(null, "Computador", 2000.00);
        Produto produto1 = new Produto(null, "Impressora", 800.00);
        Produto produto2 = new Produto(null, "Mouse", 80.00);

        categoria.getProdutos().addAll(Arrays.asList(produto));
        categoria2.getProdutos().addAll(Arrays.asList(produto1, produto));
        categoria.getProdutos().addAll(Arrays.asList(produto2));

        produto.getCategorias().addAll(Arrays.asList(categoria));
        produto1.getCategorias().addAll(Arrays.asList(categoria2));
        produto2.getCategorias().addAll(Arrays.asList(categoria));

        categoriaRepository.saveAll(Arrays.asList(categoria, categoria2));
        produtoRepository.saveAll(Arrays.asList(produto, produto1, produto2));

    }


}